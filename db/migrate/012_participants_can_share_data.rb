class ParticipantsCanShareData < ActiveRecord::Migration
  def up
    add_column :participants, :share_data, :boolean, default: false
  end

  def down
    remove_column :participants, :share_data
  end
end
