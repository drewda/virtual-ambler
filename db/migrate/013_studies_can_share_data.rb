class StudiesCanShareData < ActiveRecord::Migration
  def up
    add_column :studies, :when_to_share_data, :string, default: '3years'
  end

  def down
    drop_column :studies, :when_to_share_data
  end
end
