class FirstDataModel < ActiveRecord::Migration
  def self.up
    create_table :experimenters do |t|
      t.string :first_name
      t.string :last_name
      t.string :web_site
      t.string :country
      t.integer :lab_id
      t.boolean :administrator
      t.database_authenticatable
      t.recoverable
      t.rememberable
      t.trackable
      t.invitable
      t.timestamps
    end
    add_index :experimenters, :invitation_token # for invitable
    create_table :labs do |t|
      t.string :name
      t.string :slug
      t.string :web_site
      t.string :country
      t.timestamps
    end
    create_table :studies do |t|
      t.string :name
      t.integer :lab_id
      t.integer :experimenter_id
      t.boolean :active
      t.boolean :ask_participant_id
      t.boolean :ask_name
      t.boolean :ask_email
      t.boolean :ask_age
      t.boolean :ask_sex
      t.text :welcome_text
      t.text :completion_text
      t.timestamps
    end
    create_table :instrument_in_studies do |t|
      t.integer :study_id
      t.integer :order
      t.string :instrument
      t.string :version
      t.boolean :force_wait
      t.boolean :randomize
      t.timestamps
    end
    create_table :participants do |t|
      t.string :identification
      t.integer :study_id
      t.string :age
      t.string :sex
      t.timestamps
    end
    create_table :participant_personal_records do |t|
      t.integer :participant_id
      t.string :first_name
      t.string :last_name
      t.string :email
      t.timestamps
    end
    create_table :sbsod_records do |t|
      t.integer :participant_id
      t.integer :q1
      t.integer :q2
      t.integer :q3
      t.integer :q4
      t.integer :q5
      t.integer :q6
      t.integer :q7
      t.integer :q8
      t.integer :q9
      t.integer :q10
      t.integer :q11
      t.integer :q12
      t.integer :q13
      t.integer :q14
      t.integer :q15
      t.boolean :randomized
      t.timestamps
    end
    create_table :mrt_records do |t|
      t.integer :participant_id
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :q5
      t.string :q6
      t.string :q7
      t.string :q8
      t.string :q9
      t.string :q10
      t.string :q11
      t.string :q12
      t.string :q13
      t.string :q14
      t.string :q15
      t.string :q16
      t.string :q17
      t.string :q18
      t.string :q19
      t.string :q20
      t.boolean :force_wait
      t.timestamps
    end
    create_table :pf_records do |t|
      t.integer :participant_id
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :q5
      t.string :q6
      t.string :q7
      t.string :q8
      t.string :q9
      t.string :q10
      t.string :q11
      t.string :q12
      t.string :q13
      t.string :q14
      t.string :q15
      t.string :q16
      t.string :q17
      t.string :q18
      t.string :q19
      t.string :q20
      t.boolean :force_wait
      t.timestamps
    end
    create_table :vv_records do |t|
      t.integer :participant_id
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :q5
      t.string :q6
      t.string :q7
      t.string :q8
      t.string :q9
      t.string :q10
      t.string :q11
      t.string :q12
      t.string :q13
      t.string :q14
      t.string :q15
      t.string :q16
      t.string :q17
      t.string :q18
      t.string :q19
      t.string :q20
      t.string :q21
      t.string :q22
      t.string :q23
      t.string :q24
      t.timestamps
    end
    create_table :pvas_records do |t|
      t.integer :participant_id
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :q5
      t.string :q6
      t.string :q7
      t.string :q8
      t.string :q9
      t.string :q10
      t.boolean :randomized
    end
    create_table :psas_records do |t|
      t.integer :participant_id
      t.string :q1
      t.string :q2
      t.string :q3
      t.string :q4
      t.string :q5
      t.string :q6
      t.string :q7
      t.string :q8
      t.string :q9
      t.string :q10
      t.string :q11
      t.string :q12
      t.string :q13
      t.string :q14
      t.string :q15
      t.string :q16
      t.string :q17
      t.string :q18
      t.string :q19
      t.string :q20
      t.string :q21
      t.string :q22
      t.string :q23
      t.string :q24
      t.string :q25
      t.string :length
      t.boolean :randomized
    end
    create_table :log_entries do |t|
      t.string :type
      t.integer :user_id
      t.string :note
      t.timestamps
    end
  end

  def self.down
    drop_table :experimenters
    drop_table :labs
    drop_table :studies
    drop_table :instrument_in_studies
    drop_table :participants
    drop_table :participant_personal_records
    drop_table :sbsod_records
    drop_table :mrt_records
    drop_table :pf_records
    drop_table :vv_records
    drop_table :log_entries
  end
end
