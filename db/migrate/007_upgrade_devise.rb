class UpgradeDevise < ActiveRecord::Migration
  def change
  	remove_column :experimenters, :remember_token
  	add_column :experimenters, :reset_password_sent_at, :datetime
  end
end
