class AddLabManager < ActiveRecord::Migration
  def change
    add_column :experimenters, :lab_manager, :boolean, :default => false
  end
end
