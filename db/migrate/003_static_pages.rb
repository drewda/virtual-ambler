class StaticPages < ActiveRecord::Migration
  def self.up
    create_table :static_pages do |t|
      t.integer :study_id
      t.string :name
      t.text :body
      t.string :buttons
      t.timestamps
    end
    add_column :instrument_in_studies, :static_page_id, :integer
  end
  def self.down
    drop_table :static_pages
    remove_column :instrument_in_studies, :static_page_id
  end
end