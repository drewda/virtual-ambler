class VamblerAdditions < ActiveRecord::Migration
  def self.up
    create_table :virtual_environments do |t|
      t.string :name
      t.integer :map_arrangement_pixel_width
      t.integer :map_arrangement_pixel_height
      t.timestamps
    end
    create_table :virtual_landmarks do |t|
      t.string :name
      t.string :map_arrangement_piece_image
      t.string :icon_image
      t.integer :piece_upper_left_pixel_x
      t.integer :piece_upper_left_pixel_y
      t.integer :front_door_pixel_x
      t.integer :front_door_pixel_y
      t.integer :pointing_location_pixel_x
      t.integer :pointing_location_pixel_y
      t.integer :virtual_route_id
      t.integer :virtual_environment_id
      t.integer :visit_order
      t.timestamps
    end
    create_table :virtual_routes do |t|
      t.string :name
      t.timestamps
    end
    create_table :virtual_map_arrangements do |t|
      t.integer :participant_id
      t.integer :virtual_environment_id
      t.integer :seconds_taken
      t.timestamps
    end
    create_table :virtual_map_arrangement_pieces do |t|
      t.integer :virtual_map_arrangement_id
      t.integer :virtual_landmark_id
      t.integer :upper_left_pixel_x
      t.integer :upper_left_pixel_y
      t.integer :placement_order
      t.timestamps
    end
    create_table :virtual_distance_tests do |t|
      t.integer :participant_id
      t.integer :virtual_environment_id
      t.integer :seconds_taken
      t.timestamps
    end
    create_table :virtual_distance_estimates do |t|
      t.integer :virtual_distance_test_id
      t.integer :start_landmark_id
      t.integer :target_landmark_id
      t.integer :anchor_landmark_id
      t.integer :target_pixel_position
      t.timestamps
    end
    create_table :virtual_direction_tests do |t|
      t.integer :participant_id
      t.integer :virtual_environment_id
      t.string :style
      t.integer :seconds_taken
      t.timestamps
    end
    create_table :virtual_direction_estimates do |t|
      t.integer :virtual_direction_test_id
      t.integer :start_landmark_id
      t.integer :facing_landmark_id
      t.integer :target_landmark_id
      t.integer :bearing
      t.timestamps
    end
    create_table :vambler_demographics_records do |t|
      t.integer :participant_id
      t.string :gender
      t.string :ethnic_category
      t.string :racial_category
      t.string :education
      t.string :handedness
      t.string :first_language
      t.text :prescription_meds
      t.text :head_injuries
      t.timestamps
    end
    add_column :participants, :vambler_main_routes_order, :string
    add_column :participants, :vambler_connector_routes_order, :string
  end
  def self.down
    drop_table :virtual_environments
    drop_table :virtual_landmarks
    drop_table :virtual_routes
    drop_table :virtual_map_arrangements
    drop_table :virtual_map_arrangement_pieces
    drop_table :virtual_distance_tests
    drop_table :virtual_distance_estimates
    drop_table :virtual_direction_tests
    drop_table :virtual_direction_estimates
    remove_column :participants, :vambler_main_routes_order
    remove_column :participants, :vambler_connector_routes_order
  end
end