class RemoveStaticPages < ActiveRecord::Migration
  class StaticPage < ActiveRecord::Base
    belongs_to :study
  end

  def up
    InstrumentInStudy.where(instrument: 'Static Page').each do |static_page_iis|
      static_page_iis.study.instrument_in_studies.each do |other_iis|
        other_iis.update_attributes(order: other_iis.order.to_i - 1)
      end
      static_page_iis.delete
    end
    drop_table :static_pages
    remove_column :instrument_in_studies, :static_page_id
  end

  def down
    create_table :static_pages do |t|
      t.integer :study_id
      t.string :name
      t.text :body
      t.string :buttons
      t.timestamps
    end
    add_column :instrument_in_studies, :static_page_id, :integer
  end
end
