class LimitIdentificationToIdString < ActiveRecord::Migration
  def up
    Study.all.each do |study|
      study.update_attributes(:ask_participant_id => true,
                              :ask_name => false,
                              :ask_email => false,
                              :ask_age => false,
                              :ask_sex => false)
  
    end
  end

  def down
  end
end
