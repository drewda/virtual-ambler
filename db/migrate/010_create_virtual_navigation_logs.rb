class CreateVirtualNavigationLogs < ActiveRecord::Migration
  def change
    create_table :virtual_navigation_logs do |t|
      t.references :participant, :index => true
      t.references :virtual_environment, :index => true
      t.string :route
      t.text :log
      t.timestamps
    end
  end
end
