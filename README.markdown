Spatial Ability
===============

A [Ruby on Rails](http://rubyonrails.org/)-based Web app with spatial ability tests. Written by [Drew Dara-Abrams](http://drew.dara-abrams.com). Code released under the GNU General Public License, unless otherwise noted.

Prerequisites
============

A Linux or Unix server with the following installed. 

These can probably all be found as packages on standard Unix distributions:

* [Git](http://git-scm.com/) version control system
* [Apache](http://httpd.apache.org/) 2.0 Web server
* [MySQL](http://www.mysql.com/) database server
* [Ruby](http://www.ruby-lang.org/) 1.9 interpreter
* [RubyGems](http://docs.rubygems.org/) package management for Ruby libraries

Ruby libraries

* [Phusion Passenger](https://www.phusionpassenger.com/) (a.k.a. modrails) Apache module to serve Ruby on Rails applications
* Bundler

Some issues to be aware of when installing the prerequisites:

* Make sure you have at least version 1.9 of Ruby installed. By default many systems still use version 1.8.  Many system administrators like to use a package like [RVM](https://rvm.io/) or [rbenv](https://github.com/sstephenson/rbenv) so that they can install both versions side by side.
* When installing Phusion Passenger, follow [their documentation](http://www.modrails.com/documentation/Users%20guide%20Apache.html#_installation). You will need to put some lines into Apache's configuration files.


Installation
============
1. Clone this repository to a directory on your server. It will be served by Apache as a virtual host, so you might want to put it near your Web server's document directory.
2. Create a MySQL database called `vambler_production` and create a new user that has all permissions on this database. Put the user name and password into the file called `config/database.yml` in the fields that currently read `USERGOESHERE` and `PASSWORDGOESHERE`.
4. Install the Ruby gems that this Rails app depends on: 
    bundle install
