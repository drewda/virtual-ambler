set :ssh_options, {
  user: 'ddg743',
  forward_agent: false,
  auth_methods: %w(password)
}

server ENV['VAMBLER_DEPLOY_HOST'], {
  user: ENV['VAMBLER_DEPLOY_USER'],
  password: ENV['VAMBLER_DEPLOY_PASSWORD'],
  roles: %w{web app db}
}
