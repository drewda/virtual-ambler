Spatialability::Application.routes.draw do
  root :to => 'site#land'
  
  match '/lab/:id' => 'lab#land', :as => :lab_land
  match '/study/:study_id/finish' => 'study#finish', :as => :study_finish
  match '/study/:study_id/instrument/:instrument_id' => 'study#instrument', :as => :study_instrument
  match '/study/:study_id' => 'study#start', :as => :study_start
  
  devise_for :experimenters
  
  namespace :experimenter do
    root :to => 'site#dashboard'
    match 'shared_data', to: 'site#shared_data', as: :shared_data, via: :get
    resources :experimenters
    resources :labs
    resources :studies do
      get 'data', :on => :member
      resources :participants do
        resources :virtual_navigation_logs, :only => :show
      end
    end
  end
end
