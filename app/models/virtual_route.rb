class VirtualRoute < ActiveRecord::Base
  belongs_to :virtual_environment
  has_many :virtual_landmarks, :order => "visit_order ASC"
end