class VirtualEnvironment < ActiveRecord::Base
  has_many :virtual_landmarks, :order => "visit_order ASC"
  has_many :virtual_routes
  has_many :virtual_map_arrangements
  has_many :virtual_distance_tests
  has_many :virtual_direction_tests
end