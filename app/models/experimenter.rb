class Experimenter < ActiveRecord::Base
  belongs_to :lab
  has_many :studies
  
  devise :database_authenticatable, :recoverable, :rememberable, :trackable

  attr_accessible :first_name, :last_name, :email, :password, :password_confirmation, :lab_id, :administrator, :lab_manager
  
  validates_presence_of :first_name, :last_name, :email, :lab
  validates_uniqueness_of :email
  
  def full_name
    "#{first_name} #{last_name}"
  end

  # for use by simple_form
  def name
  	full_name
  end
end
