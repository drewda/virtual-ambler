class VirtualDirectionTest < ActiveRecord::Base
  belongs_to :participant
  belongs_to :virtual_environment
  belongs_to :virtual_direction_test
  has_many :virtual_direction_estimates, :dependent => :destroy
end