class Study < ActiveRecord::Base
  belongs_to :lab
  belongs_to :experimenter
  has_many :instrument_in_studies, :order => "instrument_in_studies.order ASC", :dependent => :destroy
  has_many :participants, :order => "participants.created_at DESC", :dependent => :destroy
  
  validates_presence_of :name, :lab, :experimenter

  accepts_nested_attributes_for :instrument_in_studies, :allow_destroy => true

  after_initialize :default_values
  def default_values
    self.ask_participant_id ||= true
  end
  
  def last_administered
    if p = participants.first
      p.created_at
    else
      return nil
    end
  end
end