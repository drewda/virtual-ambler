class Experimenter::StudiesController < ApplicationController
  before_filter :authenticate_experimenter!

  def index
    if current_experimenter.administrator
      @studies = Study.where('')
    else
      @studies = current_experimenter.lab.studies
    end
  end
  
  def new
    @study = Study.new
  end
  
  def show
    @study = Study.find(params[:id])
  end

  def data
    @study = Study.find(params[:id])

    if params[:table]
      @table = params[:table]
    else
      @table = 'spatial-ability-and-demographics'
    end
  end
  
  def edit
    @study = Study.find(params[:id])
  end
  
  def create
    @study = Study.new(params[:study])

    respond_to do |format|
      if @study.save
        flash[:success] = "Study record created for <strong>#{@study.name}</strong>."
        format.html { redirect_to(experimenter_study_url(@study)) }
      else
        format.html { render :action => "new" }
      end
    end
  end
  
  def update
    @study = Study.find(params[:id])

    respond_to do |format|
      if @study.update_attributes(params[:study])
        flash[:success] = "Study record updated for <strong>#{@study.name}</strong>."
        format.html { redirect_to(experimenter_study_url(@study)) }
      else
        format.html { render :action => "edit" }
      end
    end
  end
  
  def destroy
    @study = Study.find(params[:id])
    @study.destroy

    respond_to do |format|
      flash[:success] = "Study record deleted for <strong>#{@study.name}</strong>."
      format.html { redirect_to(experimenter_studies_url) }
    end
  end
end